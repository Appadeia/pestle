#!/usr/bin/env python3

import discord
import utils.bot

import bugzilla

def split_bz(str):
    return str.split("#")

async def bugzilla_message(api, tag, message, generic=False, color=3161692, url=None):
    bzapi = None
    if not generic:
        bzapi = bugzilla.Bugzilla(api)
        split_str = split_bz(tag)
        id = split_str[1]
    else:
        split_str = split_bz(tag)
        bzapi = bugzilla.Bugzilla(split_str[1])
        id = split_str[2]

    bug = bzapi.getbug(int(id))
    summary = ""

    bug_author_realname = None
    try:
        bug_author_realname = bug.creator_detail["real_name"]
    except:
        bug_author_realname = None

    bug_author = bug.creator
    if bug_author_realname is not None:
        bug_author = "**{}** ({})".format(bug_author_realname, bug_author)
    bug_summary = bug.summary
    bug_status = bug.status
    bug_severity = bug.severity
    bug_priority = bug.priority
    bug_version = bug.version
    bug_product = bug.product
    bug_component = bug.component
    bug_url = bug.weburl

    embed = discord.Embed(title=bug_summary, url=bug_url, color=color)
    embed.add_field(name="Status",    value=bug_status)
    embed.add_field(name="Product",   value=bug_product)
    embed.add_field(name="Component", value=bug_component)
    embed.add_field(name="Severity",  value=bug_severity)
    embed.add_field(name="Priority",  value=bug_priority)
    embed.add_field(name="Version",   value=bug_version)

    if url is not None:
        embed.set_footer(text=tag, icon_url=url)
    else:
        embed.set_footer(text=tag)

    await message.channel.send(embed=embed)

async def zilla(message: discord.Message):
    """
    Looks for Bugzilla tags in messages and sends information about mentioned bugs.
    """
    words = message.content.split()
    if "boo#" in message.content:
        for i in words:
            if "boo#" in i:
                await bugzilla_message("bugzilla.opensuse.org", i, message, color=7584293, url="https://lcp.world/images/Trackers/openSUSE.svg.png")
    
    if "rhbz#" in message.content:
        for i in words:
            if "rhbz#" in i:
                await bugzilla_message("bugzilla.redhat.com", i, message, color=13369344, url="https://lcp.world/images/Trackers/RedHat.svg.png")

    if "mgabz#" in message.content or "mga#" in message.content:
        for i in words:
            if "mgabz#" in i or "mga#" in i:
                await bugzilla_message("bugs.mageia.org", i, message, color=2332628, url="https://lcp.world/images/Trackers/Mageia.svg.png")

    if "kdebz#" in message.content:
        for i in words:
            if "kdebz#" in i:
                await bugzilla_message("bugs.kde.org", i, message, color=1939955, url="https://gitlab.com/hellcp/hellcp.gitlab.io/raw/ab9a06cc5edaad5b7f0fbc5249731e0e535e7efe/images/trackers/KDE.png")

    if "bsc#" in message.content:
        for i in words:
            if "bsc#" in i:
                await bugzilla_message("bugzilla.suse.com", i, message, color=185183, url="https://lcp.world/images/Trackers/SUSE.svg.png")

    if "bz#" in message.content:
        for i in words:
            if "bz#" in i:
                await bugzilla_message("generic", i, message, generic=True)

utils.bot.barista.register_message_listener(zilla)