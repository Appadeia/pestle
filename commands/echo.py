#!/usr/bin/env python3

import discord
import utils.cmd
import utils.bot

async def echo_message(message: discord.Message):
    """
    Echo the arguments. Syntax: sudo echo args
    """
    cmd = utils.cmd.lex_command(message)

    embed = discord.Embed()
    embed.colour = cmd.author.color
    embed.set_author(name="{} says...".format(cmd.author.display), icon_url=cmd.author.avatar)
    embed.description = cmd.query.content

    await cmd.se(embed)
    return

utils.bot.barista.register_command(("sudo echo", "su echo"), echo_message)