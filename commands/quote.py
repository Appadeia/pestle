#!/usr/bin/env python3

import discord

import utils.cmd
import utils.bot

async def quote_message(msg: discord.Message):
    """
    Quote and reply to a message.
    """
    cmd: utils.cmd.LexedCommand = utils.cmd.lex_command(msg)
    helpmsg = """
```dsconfig
# Syntax: sudo quote --message id --channel id
( --message | -m )
\tThe message ID to quote. Required.
( --channel | -c )
\tThe channel ID of the message. Required.
( --reply | -r )
\tReply to the message being quoted.
```
    """

    if not cmd.query.array:
        await cmd.st("You didn't pass any arguments!\n\n" + helpmsg)
        return

    mid = cmd.get_flag_pair("-m", "--message")
    cid = cmd.get_flag_pair("-c", "--channel")

    if mid is None or cid is None:
        await cmd.st("You're missing some required flags!\n\n" + helpmsg)
        return

    guild: discord.Guild = msg.guild
    channel: discord.Channel = guild.get_channel(int(cid))
    quoted_msg: discord.Message = await channel.fetch_message(int(mid))

    reply = cmd.get_flag_pair("-r", "--reply")

    embed = discord.Embed()
    embed.timestamp = quoted_msg.created_at
    embed.color = quoted_msg.author.color
    embed.title = "{} said...".format(quoted_msg.author.display_name)
    embed.description = "{}".format(quoted_msg.clean_content)

    if reply is not None:
        embed.add_field("{} replied...".format(msg.author.display_name), reply)

    await cmd.se(embed)

utils.bot.barista.register_command(("sudo quote", "su quote"), quote_message)