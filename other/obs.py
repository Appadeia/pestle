#!/usr/bin/env python3

import discord
import utils.bot

import libxml2
import requests

import re 
  
def find_urls(string: str): 
    url = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', string) 
    return url

def is_int(s: str):
    try:
        int(s)
        return True
    except:
        return False

async def obs_message(message: discord.Message):
    """
    Looks for OBS submit requests and sends information on them.
    """
    words = message.content.split()
    sr_ids = []
    url_ids = []
    
    if "sr" in message.content.lower():
        for index, word in enumerate(words):
            if "sr" in word.lower():
                if len(word) != 2:
                    if is_int(word[2:]):
                        # 'SR1234'
                        sr_ids.append(int(word[2:]))
                    elif is_int(word[3:]):
                        # 'SR#1234'
                        sr_ids.append(int(word[3:]))
                else:
                    if is_int(words[index+1]):
                        # 'SR 1234'
                        sr_ids.append(int(words[index+1]))

    for i in find_urls(message.content.lower()):
        if message.author.bot:
            break
        components = i.split("/")
        if "build.opensuse.org" in i and "request/show" in i:
            sr_ids.append(components[-1]) 
            url_ids.append(components[-1])
    
    for i in sr_ids:
        auth_creds = ('zyp_user', 'zyp_pw_1')

        try: 
            r = requests.get('https://api.opensuse.org/request/' + str(i), auth=auth_creds)
            doc = libxml2.parseDoc(r.text)
            context = doc.xpathNewContext()
            res = context.xpathEval("/request/description")
            description = res[0].getContent()
            res = context.xpathEval("/request/state/@name")
            state = res[0].getContent()
            res = context.xpathEval("/request/action/@type")
            action = res[0].getContent()
            res = context.xpathEval("/request/action/source/@project")
            source = res[0].getContent()
            res = context.xpathEval("/request/action/source/@package")
            source_pkg = res[0].getContent()
            res = context.xpathEval("/request/action/target/@project")
            target = res[0].getContent()
            res = context.xpathEval("/request/action/target/@package")
            target_pkg = res[0].getContent()

            embed = discord.Embed()
            embed.color = 3422784
            embed.title = "**SR#{}** - {} - **{}**\n".format(str(i),description,state.capitalize()) 
            embed.description = "{} **{}**:**{}** ⇒ **{}**:**{}**".format(action.capitalize(), source, source_pkg, target, target_pkg)

            if i in url_ids:
                await message.channel.send(embed=embed)
            else:
                await message.channel.send("https://build.opensuse.org/request/show/{}".format(str(i)), embed=embed)
        except:
            return

utils.bot.barista.register_message_listener(obs_message)