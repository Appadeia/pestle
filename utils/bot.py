import discord
import os
from os import path
import configparser
import importlib

barista = None
config = configparser.ConfigParser()

class Barista(discord.Client):
    command_listeners = {}
    message_listeners = []
    reaction_listeners = []
    mods = []

    def register_command(self, names, func):
        for name in names:
            self.command_listeners[name] = func
    def register_message_listener(self, func):
        self.message_listeners.append(func)
    def register_reaction_listener(self, func):
        self.reaction_listeners.append(func)

    def reload(self, s: str):
        importlib.reload(self.mods[int(s)])

    async def handle_command_message(self, message: discord.Message):
        cmd = ' '.join(message.content.split()[:2])
        func = self.command_listeners.get(cmd, None)
        if func is not None:
            await func(message)

    async def on_guild_join(self, guild: discord.Guild):
        if guild.id == 264445053596991498:
            return
        chan: discord.TextChannel = guild.text_channels[0]
        print(chan)
        if chan is None:
            await guild.leave()
            return
        cont = """
Hi! I'm Pestle!
To use me, I'll want you to do some things for me.
Say something when you want me to continue.
"""
        await chan.send(content=cont)
        msg = None
        try:
            msg = await self.wait_for("message", check=lambda message: message.channel == chan, timeout=120)
            if msg.author.id == 249987062084665344:
                await chan.send(content="Skipping setup...")
                return
        except:
            await chan.send(content="Sorry, I can't hang around in servers that don't meet my requirements. Goodbye!")
            await guild.leave()
            return
        cont = """
Just do these things, okay?
- Please don't have me with any bots that are not fully open source.
- Don't use me to do anything illegal.
- As far as the law allows, I'm not responsible for anything you do with me.
Say "okay" if you agree.
"""
        await chan.send(content=cont)
        try:
            msg = await self.wait_for("message", check=lambda message: message.channel == chan and "okay" in message.content.lower(), timeout=120)
        except:
            await chan.send(content="Sorry, I can't hang around in servers that don't meet my requirements. Goodbye!")
            await guild.leave()
            return
        await chan.send(content="Alright, welcome to Pestle!")
    async def on_raw_reaction_add(self, event: discord.RawReactionActionEvent):
        for func in self.reaction_listeners:
            await func(event)

    async def on_message(self, message: discord.Message):
        await self.handle_command_message(message)

        for func in self.message_listeners:
            await func(message)

    async def on_message_edit(self, before, after):
        await self.handle_command_message(after)

    async def on_ready(self):
        print('Barista Discord loop initialized.')
        
        print('Loading commands...')
        import commands.echo
        import commands.dnf
        import commands.stash
        import commands.rolelist
        import commands.rolemenu
        import commands.help_and_about
        import commands.guildlist
        import commands.quote
        import commands.bash
        import commands.settings
        import commands.moderation
        import commands.embed
        import commands.profile

        print('Loading message handlers...')
        import other.messagelogging
        import other.zillas
        import other.phabricator
        import other.obs
#       import other.autoslowmode

        self.mods = [commands.echo, commands.dnf, other.messagelogging, other.zillas, other.phabricator, other.obs]

        l = []
        for key, value in self.command_listeners.items():
            if value not in l:
                l.append(value)
        print('{} command listeners registered.'.format(len(l)))
        for i in l:
            if i.__doc__ is not None:
                print ("\t{}\n\t\t\t{}".format(i.__name__, i.__doc__.strip()))
            else:
                print ("\t{}".format(i.__name__))
        print('{} message listeners registered.'.format(len(self.message_listeners)))
        for i in self.message_listeners:
            if i.__doc__ is not None:
                print ("\t{}\n\t\t\t{}".format(i.__name__, i.__doc__.strip()))
            else:
                print ("\t{}".format(i.__name__))

        print('Barista is ready to go! Logged on as user {}'.format(self.user))

async def reload_cmd(message: discord.Message):
    """
    Reload module. Syntax: sudo reload mod_index
    """
    global barista
    id = message.content.split()[2]
    barista.reload(id)

def main():
    global config
    global barista

    if path.exists("config.ini"):
        config.read("config.ini")
    else:
        config['Discord'] = {}
        config['Discord']['Token'] = "token-here"
        
        config['Dnf']['Logs'] = os.getcwd() + "/dnf/logs"
        config['Dnf']['Cache'] = os.getcwd() + "/dnf/cache"

        config['Repos']['Fedora'] = os.getcwd() + "/repos/fedora"
        with open('config.ini', 'w') as configfile:
            config.write(configfile)
        exit()

    barista = Barista()
    barista.register_command(("sudo reload", "su reload"), reload_cmd)    
    barista.run(config['Discord']['Token'])
