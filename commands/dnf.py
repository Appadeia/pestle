#!/usr/bin/env python3

import discord

import utils.cmd
import utils.bot

import dnf
import dnf.base
import dnf.conf
import dnf.const
import hawkey

import logging
import traceback

fedora_dnf_obj = dnf.Base()
opensuse_dnf_obj = dnf.Base()

async def fedora_message(message: discord.Message):
    """
    Searches the Fedora repos. Syntax: dnf search args
    """
    global fedora_dnf_obj

    cmd = utils.cmd.lex_command(message)

    if cmd.query.length == 0:
        await cmd.st("Not enough arguments!")
        return

    dnf_query_obj = fedora_dnf_obj.sack.query()
    available_pkgs = dnf_query_obj.available()
    available_pkgs = available_pkgs.filter(name__substr=cmd.query.content,arch=["noarch","x86_64"])

    pkgs = []

    for pkg in available_pkgs:
        pkgs.append(" • `{}` - {}\n".format(pkg.name, pkg.summary))

    embed = discord.Embed()
    embed.color = 743332
    embed.set_footer(text="Fedora Package Search", icon_url="https://fedoraproject.org/w/uploads/archive/e/e5/20110717032101%21Fedora_infinity.png")
    embed.title = "{} search results for **{}** in Fedora".format(len(pkgs), cmd.query.content)
    embed.description = "".join(pkgs[:5])

    await cmd.se(embed)

async def opensuse_message(message: discord.Message):
    """
    Searches the openSUSE repos. Syntax: dnf search args
    """
    global opensuse_dnf_obj

    cmd = utils.cmd.lex_command(message)

    if cmd.query.length == 0:
        await cmd.st("Not enough arguments!")
        return

    dnf_query_obj = opensuse_dnf_obj.sack.query()
    available_pkgs = dnf_query_obj.available()
    available_pkgs = available_pkgs.filter(name__substr=cmd.query.content,arch=["noarch","x86_64"])

    pkgs = []

    for pkg in available_pkgs:
        pkgs.append(" • `{}` - {}\n".format(pkg.name, pkg.summary))

    embed = discord.Embed()
    embed.color = 7584293
    embed.set_footer(text="openSUSE Package Search", icon_url="https://lcp.world/images/Trackers/openSUSE.svg.png")
    embed.title = "{} search results for **{}** in openSUSE".format(len(pkgs), cmd.query.content)
    embed.description = "".join(pkgs[:5])

    await cmd.se(embed)

fedora_dnf_obj.conf.ignorearch = True

opensuse_dnf_obj.conf.ignorearch = True

fedora_dnf_obj.conf.logdir = utils.bot.config['Dnf']['Logs']
fedora_dnf_obj.keepcache = True
fedora_dnf_obj.conf.cachedir = utils.bot.config['Dnf']['Cache']

opensuse_dnf_obj.conf.logdir = utils.bot.config['Dnf']['Logs']
opensuse_dnf_obj.keepcache = True
opensuse_dnf_obj.conf.cachedir = utils.bot.config['Dnf']['Cache']

arch = hawkey.detect_arch()

fedora_dnf_obj.conf.substitutions['arch'] = arch
fedora_dnf_obj.conf.substitutions['basearch'] = dnf.rpm.basearch(arch)
fedora_dnf_obj.conf.substitutions['releasever'] = '30'
fedora_dnf_obj.conf.module_platform_id = 'platform:f{releasever}'.format(releasever=fedora_dnf_obj.conf.substitutions['releasever'])
fedora_dnf_obj.conf.reposdir = utils.bot.config['Repos']['Fedora'] # utils.bot.config['Repos']['Fedora']
fedora_dnf_obj.conf.zchunk = False # SUSE libsolv doesn't use libzck for zchunk support

print("Loading Fedora repos...")

try:
    fedora_dnf_obj.read_all_repos()
    fedora_dnf_obj.fill_sack(load_system_repo=False)
    utils.bot.barista.register_command(("dnf search", "dnf se"), fedora_message)
except Exception as e:
    print("Failed to load Fedora repos.")
    print(e)

print("Loaded Fedora repos.")

print("Loading openSUSE repos...")

opensuse_dnf_obj.conf.reposdir = utils.bot.config['Repos']['openSUSE']
opensuse_dnf_obj.conf.zchunk = False # SUSE libsolv doesn't use libzck for zchunk support
try:
    opensuse_dnf_obj.read_all_repos()
    opensuse_dnf_obj.fill_sack(load_system_repo=False)
    utils.bot.barista.register_command(("zypper search", "zypper se"), opensuse_message)
except Exception as e:
    print("Failed to load openSUSE repos.")
    print(e)

print("Loaded openSUSE repos.")