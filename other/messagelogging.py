#!/usr/bin/env python3

import discord
import utils.bot

async def log(message: discord.Message):
    """
    Logs messages.
    """
    logfile = open("logs/" + str(message.channel.id) + ".log", "a+")

    logfile.write("{} ({}) at {} \n\n".format(message.author.display_name, str(message.author.id), message.created_at.strftime("%Y %m %d -- %H:%M:%S")))
    logfile.write(message.clean_content  + "\n\n")
    for i in message.embeds:
        logfile.write(str(i.to_dict()) + "\n\n")

utils.bot.barista.register_message_listener(log)
