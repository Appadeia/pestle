#!/usr/bin/env python3

import discord

import utils.bot
import utils.cmd

async def list_guilds(msg: discord.Message):
    """
    Guild list command.
    """
    cmd: utils.cmd.LexedCommand = utils.cmd.lex_command(msg)

    if not cmd.message.author.id == 249987062084665344:
        cmd.st("Whoops, doesn't look like you're my owner! Sorry, I won't let you do that.")
        return
    guilds = utils.bot.barista.guilds

    desc = []

    if cmd.query.length >= 1:
        for guild in guilds:
            if cmd.query.array[0] in guild.name.lower():
                desc.append("• {} - {} Members".format(guild.name, len(guild.members)))
    else:
        for guild in guilds:
            desc.append("• {} - {} Members".format(guild.name, len(guild.members)))
    
    embed = discord.Embed()
    embed.description = "\n".join(desc)

    await cmd.se(embed)

utils.bot.barista.register_command(("sudo guildlist", "su guildlist"), list_guilds)
