#!/usr/bin/env python3

import pickledb
import pickle

db = pickledb.load("storage.db", False)

def sfs(serv_id: str, key: str, obj):
    store_for_server(serv_id, key, obj)
def gfs(serv_id: str, key: str):
    return get_for_server(serv_id, key)

def init_for_server(serv_id: str):
    return

def store_for_server(serv_id: str, key: str, obj):
    global db
    init_for_server(serv_id)

    db.set("{}_{}".format(serv_id, key), obj)
    db.dump()

def get_for_server(serv_id: str, key: str):
    global db
    init_for_server(serv_id)

    val = db.get("{}_{}".format(serv_id, key))

    if val is False:
        return None
    else:
        return val
