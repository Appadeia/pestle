#!/usr/bin/env python3

import discord

import utils.bot
import utils.cmd

async def list_roles(msg: discord.Message):
    """
    Rolelist command.
    """
    cmd = utils.cmd.lex_command(msg)

    roles = msg.guild.roles

    desc = []

    if cmd.query.length >= 1:
        for role in roles:
            if cmd.query.array[0] in role.name.lower():
                desc.append("• <@&{}> - `{}`".format(role.id, role.id))
    else:
        for role in roles:
            desc.append("• <@&{}> - `{}`".format(role.id, role.id))
    
    embed = discord.Embed()
    embed.description = "\n".join(desc)

    await cmd.se(embed)

utils.bot.barista.register_command(("sudo rolelist", "su rolelist"), list_roles)