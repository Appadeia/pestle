#!/usr/bin/env python3

import discord

import utils.bot
import utils.cmd

import datetime

def init_user_profile(user_id, cmd: utils.cmd.LexedCommand):
    dict = {
        "warns": 0,
        "mutes": 0
    }
    if cmd.recall_key("{}_data".format(str(user_id))) is None:
        cmd.store_key("{}_data".format(str(user_id)), dict)

async def log_incident(user_id, action, reason, cmd: utils.cmd.LexedCommand):
    if cmd.recall_key("settings") is None:
        return
    sets: dict = cmd.recall_key("settings")
    chans: dict = sets.get("channels", {})
    modlog_id = chans.get("modlog", None)
    if modlog_id is None:
        return
    chan = cmd.guild.get_channel(int(modlog_id))
    user: discord.Member = utils.bot.barista.get_user(user_id)
    embed = discord.Embed()
    embed.set_author(name=user.display_name, icon_url=user.avatar_url)
    embed.timestamp = datetime.datetime.now()
    embed.add_field(name="Action", value=action)
    embed.add_field(name="Reason", value=str(reason))
    await chan.send(embed=embed)

async def warn(user_id, cmd: utils.cmd.LexedCommand):
    init_user_profile(user_id, cmd)
    dict = cmd.recall_key("{}_data".format(str(user_id)))
    dict["warns"] = dict["warns"] + 1
    cmd.store_key("{}_data".format(str(user_id)), dict)
    await log_incident(user_id, "warn", cmd.get_flag_pair("-r", "--reason"), cmd)

async def warn_user(msg: discord.Message):
    """
    Warn a user.
    """
    cmd: utils.cmd.LexedCommand = utils.cmd.lex_command(msg)

    if not cmd.author.mod:
        await cmd.st("You need moderator permissions to warn!")
        return
    if msg.mentions is None:
        cmd.st("You need to mention someone to warn them!")
        return
    
    for i in msg.mentions:
        init_user_profile(i.id, cmd)
        await warn(i.id, cmd)

utils.bot.barista.register_command(("sudo warn", "su warn"), warn_user)