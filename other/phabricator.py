#!/usr/bin/env python3

import discord
from phabricator import Phabricator

import utils.bot

async def revision(tag, message: discord.Message):
    phab = Phabricator()
    try:
        revision_id = tag[1:]
        revision = phab.api.differential.getrevision(revision_id=int(revision_id))
        author = phab.api.user.query(phids=[revision.authorPHID])[0]

        embed = discord.Embed(color=1939955)
        embed.set_author(name="{} ({})".format(author["realName"], author["userName"]), icon_url=author["image"])
        embed.set_footer(text="{} | Status - {}".format(tag, revision.statusName), icon_url="https://gitlab.com/hellcp/hellcp.gitlab.io/raw/ab9a06cc5edaad5b7f0fbc5249731e0e535e7efe/images/trackers/KDE.png")
        embed.title = revision.title
        embed.url = "https://phabricator.kde.org/D{}".format(revision_id)

        await message.channel.send(embed=embed)
    except:
        return

async def phabricator(message: discord.Message):
    """
    Looks for phabricator tags and returns information on mentioned objects.
    """
    words = message.content.split()
    for i in words:
        if "D" in i:
            await revision(i, message)
    return

utils.bot.barista.register_message_listener(phabricator)