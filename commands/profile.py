#!/usr/bin/env python3
import discord

import utils.bot
import utils.cmd

import pickledb

async def profile(msg: discord.Message):
    """
    User profiles.
    """
    db = pickledb.load('profile.db', True)

    cmd: utils.cmd.LexedCommand = utils.cmd.lex_command(msg)

    args_dict = {}
    sender = msg.author
    sender_id = sender.id

    helpmsg = ""
    helpmsg += "```dsconfig\n"
    helpmsg += "# Syntax: sudo profile --flag value\n"
    helpmsg += " ( --user | -u )\n"
    helpmsg += " \tGet the user specified.\n"
    helpmsg += " ( --set-desktop-environment | -w )\n"
    helpmsg += " \tSet your desktop environment or window manager.\n"
    helpmsg += " ( --set-distro | -d )\n"
    helpmsg += " \tSet your distro.\n"
    helpmsg += " ( --set-shell | -s )\n"
    helpmsg += " \tSet your shell.\n"
    helpmsg += " ( --set-editor | -e )\n"
    helpmsg += " \tSet your editor.\n"
    helpmsg += " ( --set-languages | -p )\n"
    helpmsg += " \tSet your programming languages.\n"
    helpmsg += " ( --set-blurb | -b )\n"
    helpmsg += " \tSet your profile blurb.\n"
    helpmsg += "```"

    if cmd.query.length == 0:
        await cmd.st("Not enough arguments!\n\n" + helpmsg)
        return

    desktop_query = cmd.get_flag_pair("-w", "--set-desktop-environment")
    if desktop_query is not None:
        args_dict["desktop"] = distro_query
    distro_query = cmd.get_flag_pair("-d", "--set-distro")
    if distro_query is not None:
        args_dict["distro"] = distro_query
    shell_query = cmd.get_flag_pair("-s", "--set-shell")
    if shell_query is not None:
        args_dict["shell"] = shell_query
    editor_query = cmd.get_flag_pair("-e", "--set-editor")
    if editor_query is not None:
        args_dict["editor"] = editor_query
    langs_query = cmd.get_flag_pair("-p", "--set-languages")
    if langs_query is not None:
        args_dict["langs"] = langs_query
    blurb_query = cmd.get_flag_pair("-b", "--set-blurb")
    if blurb_query is not None:
        args_dict["blurb"] = blurb_query
    user_query = cmd.get_flag_pair("-u", "--user")
    if user_query is not None:
        args_dict["user"] = user_query
            
    if args_dict == {}:
        await cmd.st("Invalid arguments!\n" + helpmsg)
    profile_updated = None
    if "desktop" in args_dict.keys():
        db.set(str(sender_id) + "_de", args_dict["desktop"])
        profile_updated = True
    if "distro" in args_dict.keys():
        db.set(str(sender_id) + "_distro", args_dict["distro"])
        profile_updated = True
    if "shell" in args_dict.keys():
        db.set(str(sender_id) + "_shell", args_dict["shell"])
        profile_updated = True
    if "editor" in args_dict.keys():
        db.set(str(sender_id) + "_editor", args_dict["editor"])
        profile_updated = True
    if "langs" in args_dict.keys():
        db.set(str(sender_id) + "_langs", args_dict["langs"])
        profile_updated = True
    if "blurb" in args_dict.keys():
        db.set(str(sender_id) + "_blurb", args_dict["blurb"])
        profile_updated = True

    if profile_updated is not None:
        await cmd.st("Profile updated!")
        return

    if "user" in args_dict.keys():
        mentioned_member = utils.cmd.get_user_closest_to_name(msg.guild, args_dict["user"])
        try:
            mentioned_member = msg.mentions[0]
        except IndexError:
            a="a"

        if mentioned_member is None:
            await cmd.st("That person does not exist. Please be more specific, or check that they exist. If you were trying to use a command, please check your syntax.")
            return
        
        mmid = mentioned_member.id

        desktop = utils.cmd.default(db.get(str(mentioned_member.id) + "_de"),"No DE/WM set.")
        distro  = utils.cmd.default(db.get(str(mentioned_member.id) + "_distro"),"No distro set.")
        shell   = utils.cmd.default(db.get(str(mentioned_member.id) + "_shell"),"No shell set.")
        editor  = utils.cmd.default(db.get(str(mentioned_member.id) + "_editor"),"No editor set.")
        langs   = utils.cmd.default(db.get(str(mentioned_member.id) + "_langs"),"No languages set.")
        blurb   = utils.cmd.default(db.get(str(mentioned_member.id) + "_blurb"), None)

        pmsg = ""
        pmsg += "> **{}'s profile**\n".format(mentioned_member.display_name)
        pmsg += "> Distro: {}\n".format(distro)
        pmsg += "> DE/WM: {}\n".format(desktop)
        pmsg += "> Shell: {}\n".format(shell)
        pmsg += "> Editor: {}\n".format(editor)
        pmsg += "> Programming Languages: {}\n".format(langs)
        if blurb is not None:
            pmsg += "> \n"
            pmsg += "> *{}*\n".format(blurb)

        await cmd.st(pmsg)

utils.bot.barista.register_command(("sudo profile", "su profile"), profile)
