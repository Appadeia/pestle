#!/usr/bin/env python3

import discord

import utils.bot
import utils.cmd
import utils.storage

def init_for_guild(cmd: utils.cmd.LexedCommand):
    if cmd.recall_key("rolemenus") is None:
        cmd.store_key("rolemenus", {})

async def rolemenu_react_handler(reaction: discord.RawReactionActionEvent):
    if utils.storage.get_for_server(str(reaction.guild_id), "rolemenus") is False:
        return
    guild = utils.bot.barista.get_guild(reaction.guild_id)
    channel = guild.get_channel(reaction.channel_id)
    message: discord.Message = await channel.fetch_message(reaction.message_id)
    member = guild.get_member(reaction.user_id)
    emoji = reaction.emoji
    emoji_id = emoji.id if emoji.is_custom_emoji() else str(emoji)

    if member == utils.bot.barista.user:
        return

    roledict = utils.storage.get_for_server(str(reaction.guild_id), "rolemenus")
    if not str(message.id) in roledict.keys():
        return
    
    messagedict = roledict[str(message.id)]
    if not str(emoji_id) in messagedict.keys():
        return

    role = discord.utils.get(guild.roles, id=int(messagedict[str(emoji_id)]))
    if role in member.roles:
        await member.remove_roles(role)
    else:
        await member.add_roles(role)
    await message.remove_reaction(emoji, member)

async def rolemenu_command(message: discord.Message):
    """
    Rolemenu management command.
    """
    cmd: utils.cmd.LexedCommand = utils.cmd.lex_command(message)
    init_for_guild(cmd)
    if cmd.author.admin is False:
        await cmd.st("You need to be an administrator to use this command!")
    query_array = cmd.query.array
    if cmd.query.length == 0:
        await cmd.st("Not enough arguments!")
    if query_array[0] == "addrole":
        query_array.pop(0)
        if not query_array:
            await cmd.st("Missing message argument! Edit your message with a valid message ID.")
        
        msg_id = query_array[0]
        reacted_message: discord.Message
        try:
            reacted_message = await cmd.message.channel.fetch_message(msg_id)
        except:
            await cmd.st("That message ID is not in this channel! Resend your command in the same message or double check your message ID.")
            return
        query_array.pop(0)
        if not query_array:
            await cmd.st("Missing role argument! Edit your message with a valid role ID.")
        if not cmd.guild_has_role_id(int(query_array[0])):
            await cmd.st("Invalid role ID! Double check your role ID and edit your message with a valid one.")
        role_id = query_array[0]
        query_array.pop(0)
        if not query_array:
            await cmd.st("Missing reaction emoji!")
        emoji = query_array[0]

        roledict: dict = cmd.recall_key("rolemenus")
        if roledict is False:
            roledict = {}
        roledict.setdefault(msg_id, {})
        roledict[msg_id][emoji] = role_id
        await reacted_message.add_reaction(emoji)
        cmd.store_key("rolemenus", roledict)

utils.bot.barista.register_reaction_listener(rolemenu_react_handler)
utils.bot.barista.register_command(("sudo rolemenu", "su rolemenu"), rolemenu_command)
