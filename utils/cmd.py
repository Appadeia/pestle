#!/usr/bin/env python3
import discord
import utils.storage
from fuzzywuzzy import process

from dataclasses import dataclass
from typing import List

lexes = {}

@dataclass
class LexedAuthor:
    display: str
    color: discord.Colour
    avatar: discord.Asset
    admin: bool
    mod: bool
    
@dataclass
class LexedQuery:
    array: List[str]
    length: int
    content: str

class LexedCommand:
    id: int
    message: discord.Message = None
    sent_message: discord.Message = None
    flags = {}
    author = LexedAuthor(None, None, None, False, False)
    query = LexedQuery([], 0, "")

    guild: discord.Guild = None

    async def se(self, embed):
        await self.send_msg(embed=embed)
    async def st(self, content):
        await self.send_msg(content=content)

    async def delete(self):
        await self.message.delete()
        global lexes

    async def send_msg(self, content=None, embed=None):
        if self.sent_message is not None:
            await self.sent_message.edit(content=content, embed=embed) 
        else:
            self.sent_message = await self.message.channel.send(content, embed=embed)

    def guild_has_role_id(self, id: int):
        for i in self.guild.roles:
            if id == i.id:
                return True
        return False
    
    def store_key(self, key, val):
        utils.storage.sfs(self.guild.id,key,val)
    def recall_key(self, key):
        return utils.storage.gfs(self.guild.id, key)
    def get_flag_pair(self, short: str, lang: str):
        if short in self.flags.keys():
            return self.flags[short]
        elif lang in self.flags.keys():
            return self.flags[lang]
        else:
            return None

    def __lex_flags(self, content: str):
        array = content.split()[2:]
        for index, i in enumerate(array,start=0):
            if "--" == i:
                self.query.content = " ".join(array[index+1:])
                break
            if "-" in i:
                val = ""
                for ii in array[index+1:]:
                    if not "-" in ii:
                        val += "{} ".format(ii)
                    else:
                        break
                self.flags[i] = val.rstrip()
        if self.flags == {}:
            self.query.content = " ".join(array)
        return

    def __init_msg(self, msg: discord.Message):
        self.message = msg

        self.query.array = get_content(self.message.content).split()
        self.query.length = len(self.query.array)

        self.author.display = self.message.author.display_name
        self.author.color = self.message.author.colour
        self.author.avatar = self.message.author.avatar_url

        self.guild = self.message.guild

        if self.message.author.guild_permissions.administrator:
            self.author.admin = True
            self.author.mod = True

        if "admin" in [y.name.lower() for y in self.message.author.roles]:
            self.author.admin = True
            self.author.mod = True

        if "moderator" in [y.name.lower() for y in self.message.author.roles]:
            self.author.mod = True

        self.__lex_flags(self.message.content)

    def relex(self, new_msg: discord.Message):
        self.__init_msg(new_msg)

    def __init__(self, message: discord.Message):
        self.id = message.id
        self.__init_msg(message)

def get_lexed_command(id: int):
    global lexes
    return lexes[int(id)]

def lex_command(message: discord.Message) -> LexedCommand:
    global lexes
    if message.id in lexes.keys():
        lexes[message.id].relex(message)
        return get_lexed_command(message.id)
    cmd = LexedCommand(message)
    lexes[cmd.id] = cmd
    return get_lexed_command(cmd.id)

def get_user_closest_to_name(guild: discord.Guild, name: str):
    members = guild.members
    member_names = []
    for i in members:
        member_names.append(i.display_name)
    closest = process.extractOne(name, member_names)
    if closest is None:
        return None
    return guild.get_member_named(closest[0])

def default(get, default):
    if get is False:
        return default
    elif get is None:
        return default
    else:
        return get

def get_content(string, trim_words=2):
    array = string.split()
    return " ".join(array[trim_words:])