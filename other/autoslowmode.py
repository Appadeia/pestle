#!/usr/bin/env python3

import discord
import utils.bot
import asyncio

slowdict = {}

async def disable_slowmode(msg: discord.Message):
    await asyncio.sleep(30)
    await msg.channel.send("💚 Starting chat... 💚")
    await msg.channel.edit(slowmode_delay=0)

async def slowmessage(msg: discord.Message):
    """
    Turns on slowmode when spam is detected.
    """
    global slowdict
    author = str(msg.author.id)
    time = msg.created_at

    if slowdict.get(author+"-lasttime", None) is not None:
        if slowdict.get(author+"-messagecount", None) is not None:
            if (time - slowdict[author+"-lasttime"]).total_seconds() > 6:
                slowdict[author+"-lasttime"] = time
                slowdict[author+"-messagecount"] = 1
            else:
                slowdict[author+"-messagecount"] += 1
                if slowdict[author+"-messagecount"] >= 5:
                    print("trigger slowmode")
                    embed = discord.Embed(color=0xFFFFFF, title="Looks like someone's spamming!")
                    embed.set_image(url="https://pics.me.me/you-do-not-spark-joy-goodbye-goodbye-42709155.png")
                    await msg.channel.send(embed=embed)
                    await msg.channel.send("🛑 Stopping chat... 🛑")
                    await msg.channel.edit(slowmode_delay=30)
                    asyncio.get_event_loop().create_task(disable_slowmode(msg))
                    
        else:
            slowdict[author+"-messagecount"] = 1
    else:
        slowdict[author+"-lasttime"] = time
        slowdict[author+"-messagecount"] = 1

utils.bot.barista.register_message_listener(slowmessage)