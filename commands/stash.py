#!/usr/bin/env python3

import discord

import utils.bot
import utils.cmd
import utils.storage

async def stash_command(msg: discord.Message):
    """
    Stash a value in storage.
    """
    cmd = utils.cmd.lex_command(msg)

    utils.storage.store_for_server(msg.guild.id, cmd.query.array[0] + str(msg.author.id), cmd.query.array[1])

    await cmd.st("Value stored!")

async def recall_command(msg: discord.Message):
    """
    Recall a value in storage.
    """

    cmd = utils.cmd.lex_command(msg)

    o = utils.storage.get_for_server(msg.guild.id, cmd.query.array[0] + str(msg.author.id))

    if o is not False:
        await cmd.st(o)
    else:
        await cmd.st("Sorry! I couldn't find that value in my archives.")

utils.bot.barista.register_command(("sudo stash", "su stash"), stash_command)
utils.bot.barista.register_command(("sudo recall", "su recall"), recall_command)