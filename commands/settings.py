#!/usr/bin/env python3

import discord

import utils.bot
import utils.cmd

async def init_settings_for_server(cmd: utils.cmd.LexedCommand):
    settings_dict = {
        "channels": {
            "modlog": None
        }
    }
    if cmd.recall_key("settings") is None:
        cmd.store_key("settings", settings_dict)

async def settings_command(msg: discord.Message):
    cmd: utils.cmd.LexedCommand = utils.cmd.lex_command(msg)
    settings = cmd.recall_key("settings")
    await init_settings_for_server(cmd)
    if cmd.author.admin is False:
        await cmd.st("You need to be an administrator to use this command!")
    if cmd.get_flag_pair("-mlc", "--modlogchannel") is not None:
        modlog_id = cmd.get_flag_pair("-mlc", "--modlogchannel")
        try:
            msg.guild.get_channel(int(modlog_id))
            settings["channels"]["modlog"] = modlog_id
            cmd.store_key("settings", settings)
            await cmd.st("Modlog channel set to <#{}>".format(modlog_id))
        except:
            await cmd.st("Invalid channel ID! Please check that your channel ID is correct.")

utils.bot.barista.register_command(("sudo settings", "su settings", "sudo sets", "su sets"), settings_command)

