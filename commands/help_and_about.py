#!/usr/bin/env python3

import discord

import utils.bot
import utils.cmd

async def about_command(msg: discord.Message):
    """
    Get some information about the bot.
    """
    members = []
    for i in utils.bot.barista.get_all_members():
        members.append(i)
    cmd = utils.cmd.lex_command(msg)
    stats = """
Guilds Joined: {}
Members Serving: {}
    """.format(len(utils.bot.barista.guilds),len(members))

    await cmd.st("Here are some stats about me:\n" + stats)

async def help_command(msg: discord.Message):
    """
    Get bot help.
    """
    cmd = utils.cmd.lex_command(msg)
    await cmd.st("Hello, I'm Pestle! See my docs at <https://appadeia.gitlab.io>!")

utils.bot.barista.register_command(("sudo about", "su about", "sudo stats", "su stats"), about_command)
utils.bot.barista.register_command(("sudo help", "su help"), help_command)