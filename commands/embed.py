#!/usr/bin/env python3

import discord

import utils.bot
import utils.cmd

from ast import literal_eval

async def embed_command(message: discord.Message):
    """
    Embed management command.
    """
    cmd: utils.cmd.LexedCommand = utils.cmd.lex_command(message)

    if not cmd.author.admin:
        cmd.st("You need author permissions to use this command!")
        return

    try:
        embed = discord.Embed.from_dict(literal_eval(cmd.query.content))
    except:
        await cmd.st("There was an error parsing the embed!")

    if not "-m" in cmd.flags.keys():
        cmd.se(embed)
    else:
        channel = cmd.flags["-m"]
        reacted_message: discord.Message
        try:
            reacted_message = await cmd.message.channel.fetch_message(int(channel))
        except:
            await cmd.st("That message ID is not in this channel! Resend your command in the same message or double check your message ID.")
            return
        await reacted_message.edit(embed=embed)

utils.bot.barista.register_command(("sudo embed", "su embed"), embed_command)