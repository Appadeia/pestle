#!/usr/bin/env python3

import discord

import utils.bot
import utils.cmd

import urllib.parse
import requests

async def bash_message(message: discord.Message):
    """
    Run a command in a virtual Bash environment.
    """
    cmd = utils.cmd.lex_command(message)

    if cmd.query.length == 0:
        await message.channel.send("Not enough arguments! Please provide a command to run!")

    urlcmd = urllib.parse.quote(cmd.query.content)

    URL = "https://rextester.com/rundotnet/api?LanguageChoice=38&Program=" + urlcmd

    r = requests.get(url=URL)
    data = r.json()
    output = ""

    if data["Warnings"] is not None:
        output += data["Warnings"]

    if data["Errors"] is not None:
        output += data["Errors"]

    if data["Result"] is not None:
        output += data["Result"]

    embed = discord.Embed()
    embed.description = """
```
{}@{} > {}
{}
```
    """.format(message.author.display_name, message.guild.name, cmd.query.content, output)

    await cmd.se(embed)
    r = None

utils.bot.barista.register_command(("bash -c", "sudo bash", "su bash"), bash_message)